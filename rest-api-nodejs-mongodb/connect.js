const mongoose = require("mongoose");

const createConnection = () => {
  //Database Connection
  return mongoose.connect(process.env.DB_CONN_STRING, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }).then((client) => {
      console.log("Connected to Database");
    })
    .catch((error) => {
      console.log(error);
    });
};

module.exports = {
  createConnection,
};
