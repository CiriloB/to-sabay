const router = require("express").Router()
const Product = require("../model/ProductModel")

const createProduct = router.post('/', async (req, res) => {

    const newProduct = new Product({
        productName: 'qwe',
        productPrice: '$12',
        productDescription: 'Pampaqweqwegana'
    })

    try {
        const savedPost = await newProduct.save()
        res.json({ created: savedPost })
    } catch (error) {
        res.status(400).json(error)
        console.log(error)
    }
})

const showProducts = router.get('', async (req, res) => {

    try {
        const products = await Product.find()
        console.log(products)
        res.send(products)
    } catch (error) {
        res.status(400).json(error)
    }

});

const deleteProductById = router.delete('/:productId', async (req, res) => {
    try {
        const productDeleted = await Product.deleteOne({ _id: req.params.productId })

        res.send( { message : 'Product Deleted'})
        console.log(productDeleted)
    } catch (error) {
        res.status(400).json(error)
        console.log(error)
    }
})

// const deleteAll = router.delete('', async (req, res) => {
//     try {
//         const productDeleted = await Product.deleteMany()

//         res.send( productDeleted )
//         console.log(productDeleted)
//     } catch (error) {
//         res.status(400).json(error)
//         console.log(error)
//     }
// })

module.exports = {
    createProduct,
    showProducts,
    deleteProductById
};