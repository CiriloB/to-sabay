const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const dotenv = require("dotenv")
const { createProduct, showProducts, deleteProductById } = require("./controller/ProductController");
const { createConnection } = require('./connect')

const PORT = 3001 || process.env.PORT;

//Middleware
app.use(express.json());
app.use(bodyParser.json());

//Enable .env
dotenv.config()

//Database
createConnection()


//Product Routes
app.use('/api/product/create', createProduct)
app.use('/api/product/showall', showProducts)
app.use('/api/product/delete', deleteProductById)
// app.use('/api/product/deleteAll', deleteAll)

app.listen(PORT, () => {console.log(`Connected to port ${PORT}`);});
